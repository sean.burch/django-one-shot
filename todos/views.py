from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm
# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todo_list/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list,
    }
    return render(request, "todo_list/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todos = form.save(False)
            form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todo_list/create.html", context)


def edit_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todos)

    context = {
        "todo_object": todos,
        "form": form,
    }
    return render(request, "todo_list/edit.html", context)
